all: build down up

build:
	docker compose build

up:
	docker compose up -d

down:
	docker compose down --remove-orphans

pre-commit-install:
	pipenv install pre-commit --dev
	pre-commit install

lint:
	pre-commit run --all-files

dev_run:
	docker compose run --volume=${PWD}/src:/src --rm --publish 8000:8000 app bash -c 'uvicorn app.internal.main:app --host 0.0.0.0 --port 8000 --reload'

dev_test:
	docker compose run --volume=${PWD}/src:/src --rm app pytest

test:
	docker compose run --rm app pytest
