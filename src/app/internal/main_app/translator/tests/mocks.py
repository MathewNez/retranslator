from httpx import Response
from pytest_mock import MockerFixture
from respx import MockRouter, Route


def mock_yacloud_service(respx_mock: MockRouter, url: str, return_value: Response) -> Route:
    return respx_mock.post(url).mock(return_value)


def mock_redis_service(mocker: MockerFixture, getter_return_value: str | None = None) -> None:
    mocker.patch('app.commons.redis.services.RedisService.get_value', return_value=(getter_return_value, None))
    mocker.patch('app.commons.redis.services.RedisService.set_value', return_value=None)
