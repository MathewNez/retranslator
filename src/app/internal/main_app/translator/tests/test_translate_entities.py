import pytest
from httpx import AsyncClient, Response
from pytest_mock import MockerFixture
from respx import MockRouter

from app.commons.errors import BaseClientError
from app.internal.main_app.translator.tests.commons import compare_error_response
from app.internal.main_app.translator.tests.mocks import mock_redis_service, mock_yacloud_service
from config import settings


@pytest.mark.asyncio
async def test_translate_entities_success(
    respx_mock: MockRouter,
    mocker: MockerFixture,
    yacloud_iam_response_success: dict,
    yacloud_translate_response_success: dict,
    iam_token_from_redis: str,
    client: AsyncClient,
):
    mock_yacloud_service(
        respx_mock,
        settings.IAM_TOKEN_API_URL,
        Response(200, json=yacloud_iam_response_success),
    )
    mock_yacloud_service(
        respx_mock,
        settings.TRANSLATE_API_URL,
        Response(200, json=yacloud_translate_response_success),
    )
    mock_redis_service(mocker, iam_token_from_redis)

    json = {'entities': [{'id': 1, 'name': 'string'}]}
    response = await client.post('/translate', json=json)
    expected_response = []
    for entity in json['entities']:
        for lang_code in settings.TRANSLATE_TO:
            expected_response.append(
                {
                    'entity_id': entity['id'],
                    'language_code': lang_code,
                    'translation': yacloud_translate_response_success['translations'][0]['text'],
                }
            )

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_translate_entities_invalid_body(
    respx_mock: MockRouter,
    mocker: MockerFixture,
    yacloud_iam_response_success: dict,
    yacloud_translate_response_success: dict,
    iam_token_from_redis: str,
    client: AsyncClient,
):
    mock_yacloud_service(
        respx_mock,
        settings.IAM_TOKEN_API_URL,
        Response(200, json=yacloud_iam_response_success),
    )
    mock_yacloud_service(
        respx_mock,
        settings.TRANSLATE_API_URL,
        Response(200, json=yacloud_translate_response_success),
    )
    mock_redis_service(mocker, iam_token_from_redis)

    json = {'id': 1, 'name': 'string'}
    response = await client.post('/translate', json=json)

    assert response.status_code == 422


@pytest.mark.asyncio
async def test_translate_entities_iam_token_error(
    respx_mock: MockRouter,
    mocker: MockerFixture,
    yacloud_response_failure: dict,
    client: AsyncClient,
):
    mock_yacloud_service(
        respx_mock,
        settings.IAM_TOKEN_API_URL,
        Response(200, json=yacloud_response_failure),
    )
    mock_yacloud_service(
        respx_mock,
        settings.TRANSLATE_API_URL,
        Response(200, json=yacloud_response_failure),
    )
    mock_redis_service(mocker)

    json = {'entities': [{'id': 1, 'name': 'string'}]}
    response = await client.post('/translate', json=json)
    compare_error_response(response.json(), BaseClientError.get_invalid_response_body_error('IAM tokens'))


@pytest.mark.asyncio
async def test_translate_entities_translation_error(
    respx_mock: MockRouter,
    mocker: MockerFixture,
    yacloud_iam_response_success: dict,
    yacloud_response_failure: dict,
    iam_token_from_redis: str,
    client: AsyncClient,
):
    mock_yacloud_service(
        respx_mock,
        settings.IAM_TOKEN_API_URL,
        Response(200, json=yacloud_iam_response_success),
    )
    mock_yacloud_service(
        respx_mock,
        settings.TRANSLATE_API_URL,
        Response(200, json=yacloud_response_failure),
    )
    mock_redis_service(mocker, iam_token_from_redis)

    json = {'entities': [{'id': 1, 'name': 'string'}]}
    response = await client.post('/translate', json=json)
    print(response.json())
    compare_error_response(response.json(), BaseClientError.get_invalid_response_body_error('Translation'))
