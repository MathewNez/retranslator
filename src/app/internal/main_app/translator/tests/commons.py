from app.commons.errors import ErrorObject


def compare_error_response(response_body: dict, error: ErrorObject):
    assert response_body['type'] == error.type
    assert response_body['message'] == error.message
