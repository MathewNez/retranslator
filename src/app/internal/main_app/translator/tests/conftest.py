import json
from datetime import datetime, timedelta

import pytest
from httpx import AsyncClient

from app.internal.main import create_app


@pytest.fixture()
def yacloud_iam_response_success() -> dict:
    return {
        'iamToken': 'token',
        'expiresAt': str(datetime.now() + timedelta(hours=12)),
    }


@pytest.fixture()
def iam_token_from_redis(yacloud_iam_response_success: dict) -> str:
    return json.dumps(yacloud_iam_response_success)


@pytest.fixture()
def yacloud_translate_response_success() -> dict:
    return {
        'translations': [
            {
                'text': 'string',
                'detectedLanguageCode': 'str',
            },
        ],
    }


@pytest.fixture()
def yacloud_response_failure() -> dict:
    return {
        'code': 1,
        'message': 'Something went wrong',
        'details': [
            {
                '@type': 'type',
                'requestId': 'uuid',
            },
        ],
    }


@pytest.fixture()
async def client() -> AsyncClient:
    app = create_app()
    async with AsyncClient(app=app, base_url='http://test') as client:
        yield client
