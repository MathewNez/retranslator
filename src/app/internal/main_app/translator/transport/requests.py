from pydantic import BaseModel

from app.internal.core.translator.dto import Entity


class EntitiesIn(BaseModel):
    entities: list[Entity]
