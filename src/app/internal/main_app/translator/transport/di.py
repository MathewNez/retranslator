from fastapi import Depends

from app.commons.redis.services import RedisService
from app.internal.core.translator.services import TranslatorService, YaTranslateClient
from app.internal.core.yacloud_auth.services import YaCloudIamClient, YaIamTokenRefresher
from config import settings


def get_redis_service() -> RedisService:
    return RedisService(settings.REDIS_HOST)


def get_iam_client(redis_service: RedisService = Depends(get_redis_service)) -> YaCloudIamClient:
    return YaCloudIamClient(settings.IAM_TOKEN_API_URL, settings.OAUTH_TOKEN, redis_service)


def get_token_refresher(
    iam_client: YaCloudIamClient = Depends(get_iam_client),
    redis_service: RedisService = Depends(get_redis_service),
) -> YaIamTokenRefresher:
    return YaIamTokenRefresher(iam_client, redis_service)


async def get_translate_client(
    token_refresher: YaIamTokenRefresher = Depends(get_token_refresher),
) -> YaTranslateClient:
    return YaTranslateClient(settings.TRANSLATE_API_URL, settings.FOLDER_ID, token_refresher)


def get_translator_service(translate_client: YaTranslateClient = Depends(get_translate_client)) -> TranslatorService:
    return TranslatorService(translate_client)
