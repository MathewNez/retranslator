from fastapi import APIRouter, Depends
from starlette.responses import JSONResponse

from app.commons.errors import get_bad_request_response
from app.commons.responses import ErrorOut
from app.internal.core.translator.dto import TranslatedEntity
from app.internal.core.translator.services import TranslatorService
from app.internal.main_app.translator.transport.di import get_translator_service
from app.internal.main_app.translator.transport.requests import EntitiesIn

translate_router = APIRouter(prefix='/translate', tags=['Translate'])


@translate_router.post(
    '',
    responses={
        200: {'model': list[TranslatedEntity]},
        400: {'model': ErrorOut},
    },
)
async def translate_entities(
    body: EntitiesIn,
    translator_service: TranslatorService = Depends(get_translator_service),
) -> JSONResponse:
    result, error = await translator_service.translate(body.entities)
    if error:
        return get_bad_request_response(error)

    return result
