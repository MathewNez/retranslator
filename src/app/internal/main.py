from fastapi import FastAPI

from app.internal.main_app.translator.transport.handlers import translate_router


def create_app() -> FastAPI:
    app_ = FastAPI()

    app_.include_router(translate_router)

    return app_


app = create_app()
