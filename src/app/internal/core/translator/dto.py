from pydantic import BaseModel


class Entity(BaseModel):
    id: int
    name: str


class TranslatedEntity(BaseModel):
    entity_id: int
    language_code: str
    translation: str
