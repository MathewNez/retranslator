from json import JSONDecodeError

import httpx
from pydantic import ValidationError

from app.commons.errors import BaseClientError, ErrorObject
from app.commons.logger import get_logger
from app.internal.core.translator.domains import Format
from app.internal.core.translator.dto import Entity, TranslatedEntity
from app.internal.core.translator.requests import TranslateBodyOut
from app.internal.core.translator.responses import YaTranslateResponse
from app.internal.core.yacloud_auth.services import YaIamTokenRefresher
from config import settings

logger = get_logger(__name__)


class YaTranslateClient:
    def __init__(
        self,
        api_url: str,
        folder_id: str,
        token_refresher: YaIamTokenRefresher,
    ):
        self._name = 'Translation'
        self._api_url = api_url
        self._folder_id = folder_id
        self._token_refresher = token_refresher

    async def translate(
        self, text: str, source_lang_code: str, target_lang_code: str
    ) -> tuple[str, None] | tuple[None, ErrorObject]:
        token_object, error = await self._token_refresher.get_actual_token()
        if error:
            return None, error
        headers = {'Authorization': f'Bearer {token_object.token}'}
        request_body = TranslateBodyOut(
            source_lang_code=source_lang_code,
            target_lang_code=target_lang_code,
            format=Format.PLAIN_TEXT,
            texts=[text],
            folder_id=self._folder_id,
        )
        async with httpx.AsyncClient() as client:
            try:
                response = await client.post(
                    self._api_url,
                    headers=headers,
                    json=request_body.model_dump(by_alias=True),
                )
            except httpx.RequestError as e:
                logger.exception(e)
                return None, BaseClientError.get_api_request_failed_error(self._name)
        try:
            body = response.json()
            result = YaTranslateResponse.model_validate(body)
        except (JSONDecodeError, TypeError, ValidationError) as e:
            logger.exception(e)
            return None, BaseClientError.get_invalid_response_body_error(self._name)

        return result.translations[0].text, None


class TranslatorService:
    def __init__(self, ya_translate_client: YaTranslateClient):
        self._ya_translate_client = ya_translate_client

    async def translate(
        self,
        entities: list[Entity],
        source_lang_code: str = settings.SOURCE_LANG_CODE,
        languages: list[str] = settings.TRANSLATE_TO,
    ) -> tuple[list[TranslatedEntity], None] | tuple[None, ErrorObject]:
        translations = []
        for entity in entities:
            for lang_code in languages:
                translation, error = await self._ya_translate_client.translate(entity.name, source_lang_code, lang_code)
                if error:
                    return None, error
                result = TranslatedEntity(entity_id=entity.id, language_code=lang_code, translation=translation)
                translations.append(result)

        return translations, None
