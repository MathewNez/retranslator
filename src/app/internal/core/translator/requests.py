from pydantic import BaseModel, Field

from app.internal.core.translator.domains import Format


class TranslateBodyOut(BaseModel):
    source_lang_code: str = Field(serialization_alias='sourceLanguageCode')
    target_lang_code: str = Field(serialization_alias='targetLanguageCode')
    format: Format
    texts: list[str]
    folder_id: str = Field(serialization_alias='folderId')
    speller: bool = False
