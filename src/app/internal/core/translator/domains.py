from enum import Enum


class Format(str, Enum):
    PLAIN_TEXT = 'PLAIN_TEXT'
