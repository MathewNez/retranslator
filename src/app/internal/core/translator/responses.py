from pydantic import BaseModel


class YaTranslation(BaseModel):
    text: str


class YaTranslateResponse(BaseModel):
    translations: list[YaTranslation]
