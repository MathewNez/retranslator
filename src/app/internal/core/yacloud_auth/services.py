from datetime import datetime
from json import JSONDecodeError

import httpx
from pydantic import ValidationError

from app.commons.errors import BaseClientError, ErrorObject
from app.commons.logger import get_logger
from app.commons.redis.services import RedisService
from app.internal.core.yacloud_auth.dto import IamTokenDTO
from app.internal.core.yacloud_auth.requests import IamRequestBodyOut
from app.internal.core.yacloud_auth.responses import IamTokenIn
from config import settings

logger = get_logger(__name__)


class YaCloudIamClient:
    def __init__(self, api_url: str, oauth_token: str, redis_service: RedisService):
        self._name = 'IAM tokens'
        self._api_url = api_url
        self._oauth_token = oauth_token
        self._redis_service = redis_service

    async def get_iam_token(self) -> tuple[IamTokenIn, None] | tuple[None, ErrorObject]:
        body = IamRequestBodyOut(oauth_token=self._oauth_token)
        async with httpx.AsyncClient() as client:
            try:
                response = await client.post(self._api_url, json=body.model_dump(by_alias=True))
            except httpx.RequestError as e:
                logger.exception(e)
                return None, BaseClientError.get_api_request_failed_error(self._name)
        try:
            body = response.json()
            token_object = IamTokenIn.model_validate(body)
        except (JSONDecodeError, TypeError, ValidationError) as e:
            logger.exception(e)
            return None, BaseClientError.get_invalid_response_body_error(self._name)

        logger.info('Requested new IAM token')
        await self._redis_service.set_value('iam_token', token_object.model_dump_json(by_alias=True))
        return token_object, None


class YaIamTokenRefresher:
    def __init__(self, iam_client: YaCloudIamClient, redis_service: RedisService):
        self._iam_client = iam_client
        self._redis_service = redis_service

    async def get_actual_token(self) -> tuple[IamTokenDTO, None] | tuple[None, ErrorObject]:
        cached_token, error = await self._redis_service.get_value('iam_token')
        if cached_token:
            iam_token = IamTokenDTO.model_validate_json(cached_token)

            if iam_token and iam_token.expires_at.replace(tzinfo=settings.TZ_INFO) > datetime.now().replace(
                tzinfo=settings.TZ_INFO
            ):
                return iam_token, None

        token, error = await self._iam_client.get_iam_token()
        if error:
            return None, error

        return token, None
