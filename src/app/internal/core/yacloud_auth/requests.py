from pydantic import BaseModel, Field


class IamRequestBodyOut(BaseModel):
    oauth_token: str = Field(serialization_alias='yandexPassportOauthToken')
