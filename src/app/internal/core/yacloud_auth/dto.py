from datetime import datetime

from pydantic import BaseModel, Field


class IamTokenDTO(BaseModel):
    token: str = Field(alias='iamToken')
    expires_at: datetime = Field(alias='expiresAt')
