from enum import Enum

from pydantic import BaseModel
from starlette.responses import JSONResponse


class ErrorObject(BaseModel):
    type: str
    message: str


class BaseClientErrorTypes(str, Enum):
    API_REQUEST_FAILED = 'api_request_failed'
    INVALID_RESPONSE_BODY = 'invalid_response_body'


class BaseClientError:
    @staticmethod
    def get_api_request_failed_error(client_name: str) -> ErrorObject:
        return ErrorObject(
            type=BaseClientErrorTypes.API_REQUEST_FAILED,
            message=f'Failed to make a request to {client_name} API',
        )

    @staticmethod
    def get_invalid_response_body_error(client_name: str) -> ErrorObject:
        return ErrorObject(
            type=BaseClientErrorTypes.INVALID_RESPONSE_BODY,
            message=f'Failed to parse response body from {client_name} API',
        )


def get_err_response(err: ErrorObject, status_code: int) -> JSONResponse:
    return JSONResponse(err.model_dump(), status_code)


def get_bad_request_response(err: ErrorObject) -> JSONResponse:
    return get_err_response(err, 400)
