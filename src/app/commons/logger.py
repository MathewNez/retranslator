from logging import Formatter, Logger, StreamHandler, getLogger

from config import settings


def get_handler(level: int | str):
    handler = StreamHandler()
    handler.setLevel(level)

    formatter = Formatter('%(asctime)s %(pathname)s:%(lineno)s %(levelname)s: %(message)s')
    handler.setFormatter(formatter)

    return handler


def get_logger(name: str, level: int | str | None = None) -> Logger:
    level = level or settings.LOG_LEVEL

    logger = getLogger(name)
    logger.setLevel(level)
    logger.addHandler(get_handler(level))

    return logger
