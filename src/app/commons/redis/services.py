from aioredis import Redis, RedisError

from app.commons.errors import BaseClientError, ErrorObject
from app.commons.logger import get_logger

logger = get_logger(__name__)


class RedisService:
    def __init__(self, host: str):
        self._name = 'Redis'
        self._redis = Redis(host=host)

    async def get_value(self, key: str) -> tuple[str | None, None] | tuple[None, ErrorObject]:
        async with self._redis.client() as client:
            try:
                return await client.get(key), None
            except RedisError as e:
                logger.exception(e)
                return None, BaseClientError.get_api_request_failed_error(self._name)

    async def set_value(self, key: str, value: str) -> None | ErrorObject:
        async with self._redis.client() as client:
            try:
                await client.set(key, value)
            except RedisError as e:
                logger.exception(e)
                return BaseClientError.get_api_request_failed_error(self._name)
