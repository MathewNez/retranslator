import logging
import os

import pytz
from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())

# Yandex Cloud credentials
OAUTH_TOKEN = os.getenv('OAUTH_TOKEN')
FOLDER_ID = os.getenv('FOLDER_ID')

# Yandex Cloud API URLs
IAM_TOKEN_API_URL = os.getenv('IAM_TOKEN_API_URL', 'https://iam.api.cloud.yandex.net/iam/v1/tokens')
TRANSLATE_API_URL = os.getenv('TRANSLATE_API_URL', 'https://translate.api.cloud.yandex.net/translate/v2/translate')

# Translation settings
SOURCE_LANG_CODE = os.getenv('SOURCE_LANG_CODE', 'en')
TRANSLATE_TO = list(map(str, os.getenv('TRANSLATE_TO', 'ar,de,en,es,fr,it').split(',')))

# Time zone settings
TZ_INFO = pytz.UTC

# Redis connection settings
REDIS_HOST = os.getenv('REDIS_HOST', 'redis')

# Logging settings
LOG_LEVEL = os.getenv('LOG_LEVEL', logging.DEBUG)
